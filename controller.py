import temp_probe
import time
import power

class Controller:
    def __init__(self, set_point, probe, switch):
        self.set_point = set_point
        self.kp = 50 
        self.kd = 0 
        self.ki = 1
        self.probe = probe
        self.power = switch


    def warm_up():
        temp = self.probe.fahrenheit()
        while temp < (set_point - 5):
            self.power.on()
            time.sleep(30)
            temp = self.probe.fahrenheit()


    def loop(self):

        self.warm_up()

        self.power.off()
        integral = 0
        prev_error = 0
        last_probe_ms = time.time()

        while True:
            temp = self.probe.fahrenheit()
            cur_probe_ms = time.time()

            dt = cur_probe_ms - last_probe_ms
            last_probe_ms = cur_probe_ms
            
            err = self.set_point - temp
            integral += dt * err
            integral += err
            der = (err - prev_error) / dt 

            out = self.kp*err + self.kd*der + self.ki*integral

            prev_error = err

            print "temp:", temp, "err:", err, "integral:", integral, "der:", der, "out:", str(out)

            for i in range(100):
                if i < out:
                    self.power.on()
                else:
                    self.power.off()
                time.sleep(1)


if __name__ == '__main__':
    c = Controller(150, temp_probe.TempProbe(), power.PowerSwitch(1))
    c.loop()
