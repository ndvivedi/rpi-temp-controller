import os
import glob
import time
 
class TempProbe:

    def __init__(self, device="/sys/bus/w1/devices/28-0000050092f3/w1_slave"):
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')
        self.device = device
 
    
    def celsius(self):
        self.read_temp()[0]

    def fahrenheit(self):
        self.read_temp()[1]


    def read_temp(self):
        with open(self.device, 'r') as f:
            lines = f.readlines()
            celsius = float(lines[1].split(' ')[-1].split('=')[1]) / 1000.0
            fahrenheit = celsius * 9.0 / 5.0 + 32.0
            return celsius, fahrenheit
        
if __name__ == '__main__':
    p = TempProbe()
    while True:
        print(p.read_temp())	
        time.sleep(1)
